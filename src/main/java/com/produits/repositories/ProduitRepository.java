package com.produits.repositories;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.produits.entities.Produits;


@Repository
public interface ProduitRepository extends JpaRepository<Produits, Integer>{

	
 
}
