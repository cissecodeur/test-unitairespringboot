package com.produits.repositories;

import java.util.List;

import com.produits.entities.Produits;

public interface ProduitInterface {

	 public List<Produits> find(Produits produit);
	 public Produits add(Produits produit);
	 public void delete(int id);
	 public Produits update(Produits produit);
}
