package com.produits;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.produits.entities.Produits;
import com.produits.repositories.ProduitRepository;




@SpringBootApplication
//@EnableSwagger2
public class ECommerceApplication implements CommandLineRunner {
	
	
	@Autowired
	private ProduitRepository produitRepository;

	public static void main(String[] args) {
		SpringApplication.run(ECommerceApplication.class, args);
	}

	
	public void run(String... args) throws Exception {
	
	produitRepository.save(new Produits(1,"Aspirine",300,600));
	produitRepository.save(new Produits(2,"Rhumagrip",200,700));
	produitRepository.save(new Produits(3,"Nivaquine",300,900));
	produitRepository.save(new Produits(4,"Doliprane",900,1000));
	
	
	produitRepository.findAll().forEach(produit-> {
	System.out.println(produit.toString());
	});
	}

}
