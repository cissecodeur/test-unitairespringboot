package com.produits.implementations.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND) //ramener le code d'erreur pour la ressource non trouvée
public class ProduitIntrouvableException extends RuntimeException {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProduitIntrouvableException (String s) {
		 super(s);
	 }
}
