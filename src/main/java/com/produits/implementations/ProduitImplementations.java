package com.produits.implementations;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.produits.entities.Produits;
import com.produits.implementations.exceptions.ProduitIntrouvableException;
import com.produits.repositories.ProduitRepository;


@RestController
@RequestMapping("/")
public class ProduitImplementations {
	
	
	@Autowired
	private ProduitRepository produitRepository ;
	
	@GetMapping("/all")
	public List<Produits> Afficher(Produits produits) {
		return produitRepository.findAll();
	}
	
	
	
    @RequestMapping("/{id}") 
    public Produits afficherUnProduit(@PathVariable int  id) {
	  Optional<Produits> produits = produitRepository.findById(id);
	  if(!produits.isPresent()) {
		  throw new ProduitIntrouvableException("ce produit avec le id " + id + " est introuvable");   
	  }
	    else {
	    	 return produits.get();
	    }
	  }
	 
	
	
	  //ajouter un produit
	
	/*
	 * @PostMapping(value = "/Produits") public ResponseEntity<?>
	 * ajouterProduit(@Valid @RequestBody Produits produit) {
	 * 
	 * Produits produitAdded = produitRepository.save(produit);
	 * 
	 * if (produitAdded == null)
	 * 
	 * return ResponseEntity.noContent().build();
	 * 
	 * URI location = ServletUriComponentsBuilder.fromCurrentRequest()
	 * .path("/{id}") .buildAndExpand(produitAdded.getIdproduit()) .toUri();
	 * 
	 * return ResponseEntity.created(location).build();
	 * 
	 * }
	 */
	 
	 
	
	
	 @PostMapping("/ajouter") 
	  public Produits ajouterProduit(Produits produits) {
	    return produitRepository.save(produits);
	  
	  }
	 
	 	
	
	@DeleteMapping("/supprimer") 
	  public void SupprimerTout(@PathVariable int id) {
		 produitRepository.deleteAll(); 
		 System.out.println("supprimé avec succes");
	  }
	 

	@DeleteMapping("/supprimer/{id}")
	public void SupprimerUnProduit(@PathVariable int id) {
		produitRepository.deleteById(id);
	}
	
	/*
	 * @PutMapping("/modifier/{id}") public ResponseEntity<Object>
	 * updateStudent(@RequestBody Produits produits, @PathVariable int id) {
	 * 
	 * Optional<Produits> produitOptional = produitRepository.findById(id);
	 * 
	 * if (!produitOptional.isPresent()) return ResponseEntity.notFound().build();
	 * 
	 * produits.setIdproduit(id);
	 * 
	 * produitRepository.save(produits);
	 * 
	 * return ResponseEntity.noContent().build(); }
	 */

}
