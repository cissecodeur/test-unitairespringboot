package com.produits.entities;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;

@Entity
public class Produits implements Serializable{
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idproduit;
	
	//@Length(min=3 , max=20 , message = "le nom du produit doit etre superieur a 3 et inferieur a 20")
	private String nomproduit;
	
	//@Min(value = 1)
	private int prixvente;
	private int prixachat;
	
	public Produits() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Produits(int idproduit, String nomproduit, int prixvente, int prixachat) {
		super();
		this.idproduit = idproduit;
		this.nomproduit = nomproduit;
		this.prixvente = prixvente;
		this.prixachat = prixachat;
	}
 
	  
	public int getIdproduit() {
		return idproduit;
	}

	public void setIdproduit(int idproduit) {
		this.idproduit = idproduit;
	}

	public String getNomproduit() {
		return nomproduit;
	}

	public void setNomproduit(String nomproduit) {
		this.nomproduit = nomproduit;
	}

	public int getPrixvente() {
		return prixvente;
	}

	public void setPrixvente(int prixvente) {
		this.prixvente = prixvente;
	}

	public int getPrixachat() {
		return prixachat;
	}

	public void setPrixachat(int prixachat) {
		this.prixachat = prixachat;
	}

	@Override
	public String toString() {
		return "Produits [idproduit=" + idproduit + ", nomproduit=" + nomproduit + ", prixvente=" + prixvente
				+ ", prixachat=" + prixachat + "]";
	}
	
	
   
	
	

}
